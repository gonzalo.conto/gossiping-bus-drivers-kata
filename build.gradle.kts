import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.31"
    `maven-publish`
}

group = "com.etermax.kotlin"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    testImplementation("junit:junit:4.11")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

publishing {
    publications {
        register("mavenJava", MavenPublication::class) {
            from(components["java"])
        }
    }
    repositories {
        maven {
            credentials {
                username = "deploys"
                password = "Luna1234\$"
            }

            val isSnapshot = project.version.toString().endsWith("-SNAPSHOT")
            val repo =if (isSnapshot) "snapshot" else "release"
            setUrl("https://mavenrepo.etermax.com/artifactory/libs-$repo-local")
        }
    }
}
