import com.etermax.template.Driver
import com.etermax.template.RouteEncounter
import org.junit.Assert.assertEquals
import org.junit.Test

class TemplateShould {

    @Test
    fun `stops in common`() {
        val firstStops = listOf(3, 3, 4)
        val routeEncounter = RouteEncounter()

        val result = routeEncounter.evaluate(firstStops)

        assertEquals(
            mapOf(
                0 to listOf(1)
            ), result
        )
    }

    @Test
    fun `stops in common with multiple encounters`() {
        val firstStops = listOf(3, 2, 3, 2, 3)
        val routeEncounter = RouteEncounter()

        val result = routeEncounter.evaluate(firstStops)

        assertEquals(
            mapOf(
                0 to listOf(2, 4),
                1 to listOf(3),
                2 to listOf(4)
            ), result
        )
    }

    @Test
    fun `drivers should share gossips`() {
        val GOSSIPS = setOf(0, 1)
        val driver = Driver(0)
        val driver2 = Driver(1)

        driver.shareGossips(driver2)

        assertEquals(driver.getGossips(), GOSSIPS)
        assertEquals(driver2.getGossips(), GOSSIPS)
    }

    @Test
    fun `multiple drivers share gossips`() {
        val driver1 = Driver(0, listOf(3, 1, 2, 3))
        val driver2 = Driver(1, listOf(3, 2, 3, 1))
        val driver3 = Driver(2, listOf(4, 2, 3, 4, 5))
        val drivers = listOf(driver1, driver2, driver3)
        val atendedorDeBoludos = AtendedorDeBoludos(drivers, RouteEncounter())

        val result = atendedorDeBoludos.atender()

        assertEquals("5", result)
    }

    @Test
    fun `multiple drivers share gossips fafafa`() {
        val driver1 = Driver(0, listOf(3, 1, 2, 3))
        val driver2 = Driver(1, listOf(3, 2, 3, 1))
        val drivers = listOf(driver1, driver2)
        val atendedorDeBoludos = AtendedorDeBoludos(drivers, RouteEncounter())

        val result = atendedorDeBoludos.atender()

        assertEquals("1", result)
    }

    @Test
    fun `multiple drivers share gossips fafafa2`() {
        val driver1 = Driver(0, listOf(3, 1, 2, 3))
        val driver2 = Driver(1, listOf(1, 2, 3, 4, 5, 6, 7, 8, 3))
        val drivers = listOf(driver1, driver2)
        val atendedorDeBoludos = AtendedorDeBoludos(drivers, RouteEncounter())

        val result = atendedorDeBoludos.atender()

        assertEquals("9", result)
    }

    @Test
    fun `driver never share all gossips`() {
        val driver1 = Driver(0, listOf(2,1,2))
        val driver2 = Driver(1, listOf(5,2,8))
        val drivers = listOf(driver1, driver2)

        val result = AtendedorDeBoludos(drivers, RouteEncounter()).atender()

        assertEquals("never", result)
    }

    @Test
    fun `driver never share all gossips fumata 1`() {
        val driver1 = Driver(0, listOf(7,11, 2, 2, 4, 8, 2, 2))
        val driver2 = Driver(1, listOf(3, 0, 11,8))
        val driver3 = Driver(2, listOf(5, 11, 8, 10, 3, 11))
        val driver4 = Driver(3, listOf(5, 9, 2, 5, 0, 3))
        val driver5 = Driver(4, listOf(7, 4, 8, 2, 8, 1, 0, 5))
        val driver6 = Driver(5, listOf(3, 6, 8, 9))
        val driver7 = Driver(6, listOf(4, 2, 11, 3, 3))
        val drivers = listOf(driver1, driver2, driver3, driver4, driver5, driver6, driver7)

        val result = AtendedorDeBoludos(drivers, RouteEncounter()).atender()

        assertEquals("9", result)
    }


}