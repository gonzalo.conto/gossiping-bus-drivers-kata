package com.etermax.template

class RouteEncounter {
    fun evaluate(routes: List<Int>): Map<Int, List<Int>> {
        var map = mutableMapOf<Int, List<Int>>()
        for (i in routes.indices) {
            val list = mutableListOf<Int>()
            for(j in i+1 until routes.size){
                if (routes[i] == routes[j])
                    list.add(j)
            }
            if (list.any())
                map[i] = list
        }
        return map
    }
}
