package com.etermax.template

data class Driver(private val gossip: Int, val paths: List<Int> = listOf()) {
    private var gossips: MutableSet<Int> = mutableSetOf(gossip)

    fun getGossips(): MutableSet<Int> {
        return gossips
    }

    fun shareGossips(anotherDriver: Driver) {
        val anotherGossips = anotherDriver.getGossips()
        gossips.addAll(anotherGossips)
        anotherDriver.giveGossips(gossips)
    }

    private fun giveGossips(otherGossips: MutableSet<Int>) {
        gossips.addAll(otherGossips)
    }
}