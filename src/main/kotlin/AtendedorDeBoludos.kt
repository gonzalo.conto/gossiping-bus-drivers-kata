import com.etermax.template.Driver
import com.etermax.template.RouteEncounter

class AtendedorDeBoludos(private val drivers: List<Driver>, private val routeEncounter: RouteEncounter) {
    fun atender(): String {

        for (i in 0 until 480){
            val firstStops = drivers.map {
                it.paths[i % it.paths.size] }
            val driversExchangingGossips = routeEncounter.evaluate(firstStops)

            driversExchangingGossips.keys
                .forEach{ driverIndex -> driversExchangingGossips[driverIndex]
                    ?.forEach { otherDriver -> drivers[driverIndex].shareGossips(drivers[otherDriver]) }
                }

            if (drivers.all { it.getGossips().size == drivers.size })
                return (i+1).toString()
        }

        return "never"
    }
}
