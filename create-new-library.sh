#!/bin/bash

read_user_params() {
    read -p "Enter your Library Name in PascalCase [eg. TemplateLibrary]: " USER_LIBRARY_NAME
    read -p "Enter your Package [eg. com.etermax.kotlin]: " USER_PACKAGE
    echo ""
}

create_or_replace_service_directory() {
    local BASE_PATH=$(cd .. && pwd)
    if [[ -d "$BASE_PATH/$ID_LIBRARY" ]]; then
        echo "Creating new project => $ID_LIBRARY"
        echo ""
        read -p "Directory $BASE_PATH/$ID_LIBRARY already exists. Do you want to override it? [y/N]" result

        if [[ "$result" == "Y" ]] || [[ "$result" == "y" ]] ; then
            rm -rf ${BASE_PATH}/${ID_LIBRARY}
        else
            echo "Exiting cli ..."
            exit
        fi
    fi

    mkdir ../${ID_LIBRARY}
}

copy_files() {
    local BASE_PATH="$(cd .. && pwd)/$ID_LIBRARY/"
    cp .gitignore ${BASE_PATH}
    cp .gitlab-ci.yml ${BASE_PATH}
    cp build.gradle.kts ${BASE_PATH}
    cp gradle.properties ${BASE_PATH}
    cp gradlew ${BASE_PATH}
    cp gradlew.bat ${BASE_PATH}
    cp settings.gradle ${BASE_PATH}
    cp -R gradle ${BASE_PATH}
    mkdir -p ${BASE_PATH}src/main/kotlin
    mkdir -p ${BASE_PATH}src/main/resources
    mkdir -p ${BASE_PATH}src/test/kotlin
    mkdir -p ${BASE_PATH}src/test/resources
}

update_files() {
    local BASE_PATH="$(cd .. && pwd)/$ID_LIBRARY/"
    sed -i '' s/'com.etermax.kotlin'/${USER_PACKAGE}/g ${BASE_PATH}build.gradle.kts
    sed -i '' s/'template'/${ID_LIBRARY}/g ${BASE_PATH}settings.gradle
}

show_help() {
    echo "$0 <LibraryName> [Package]"
    echo "    LibraryName in PascalCase [eg. TemplateLibrary]"
    echo "    Package [default: com.etermax.kotlin]"
}

#read_user_params

if [[ $# -lt 1 ]] ; then
    show_help
    exit 1
fi

echo ""
echo "  _  __     _   _ _         _     _ _ "
echo " | |/ /___ | |_| (_)_ __   | |   (_) |__  _ __ __ _ _ __ _   _"
echo " | ' // _ \| __| | | '_ \  | |   | | '_ \| '__/ _\` | '__| | | | "
echo " | . \ (_) | |_| | | | | | | |___| | |_) | | | (_| | |  | |_| | "
echo " |_|\_\___/ \__|_|_|_| |_| |_____|_|_.__/|_|  \__,_|_|   \__, | "
echo "                                                         |___/ "



USER_LIBRARY_NAME=$1
USER_PACKAGE=${2:-com.etermax.kotlin}

ID_LIBRARY=$(echo ${USER_LIBRARY_NAME} | sed 's/\(.\)\([A-Z]\)/\1-\2/g' | tr '[:upper:]' '[:lower:]')

# Remove .DS_Store files if exists to omit issues using sed.
find . -name '.DS_Store' -type f -exec rm {} \;

create_or_replace_service_directory

copy_files
update_files
